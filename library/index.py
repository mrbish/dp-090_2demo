#!/usr/bin/python3
# Turn on debug mode.
from jinja2 import FileSystemLoader, Environment, Template
import cgi, cgitb
from data import data_mysql as db
import models


cgitb.enable()

loader = FileSystemLoader('templates')
env = Environment(loader=loader)
base = env.get_template('base.html')
genre_all = env.get_template('genre_all.html')
books_all = env.get_template('books_all.html')

print('Content-Type: text/html')
print()

form = cgi.FieldStorage()

action = form.getvalue('action')

if action == 'get_all_genres':
    print(genre_all.render(books=db.Actions.get_all_books(), genres=db.Actions.get_all_genres()))

elif action == 'add_genre':
    genre_title = form.getvalue('genre_title')
    db.Actions.add_genre(genre_title)
    print(genre_all.render(books=db.Actions.get_all_books(), genres=db.Actions.get_all_genres()))

elif action == 'delete_genre':
    genre_title = form.getvalue('genre_title')
    db.Actions.delete_genre(genre_title)
    print(genre_all.render(books=db.Actions.get_all_books(), genres=db.Actions.get_all_genres()))

elif action == 'edit_genre_title':
    old_genre_title = form.getvalue('old_genre_title')
    new_genre_title = form.getvalue('new_genre_title')
    db.Actions.edit_genre_title(old_genre_title, new_genre_title)
    print(genre_all.render(books=db.Actions.get_all_books(), genres=db.Actions.get_all_genres()))

elif action == 'get_all_books':
    print(books_all.render(books=db.Actions.get_all_books(), genres=db.Actions.get_all_genres()))

elif action == 'add_book':
    book_title = form.getvalue('book_title')
    book_author = form.getvalue('book_author')
    book_year = form.getvalue('book_year')
    book_description = form.getvalue('book_description')
    book_genre = form.getvalue('book_genre')
    genres=db.Actions.get_all_genres()
    for genre in genres:
        if genre.title == book_genre:
            genre_id = genre.genre_id
    data = { 
        'book_title': book_title, 'book_author': book_author, 
        'book_year': book_year, 'book_description': book_description,
        'book_genre': book_genre, 'genre_id' : genre_id
    }
    db.Actions.add_book(data)
    print(books_all.render(books=db.Actions.get_all_books(), genres=db.Actions.get_all_genres()))

elif action == 'delete_book':
    book_title = form.getvalue('book_title')
    db.Actions.delete_book(book_title)
    print(books_all.render(books=db.Actions.get_all_books(), genres=db.Actions.get_all_genres()))


elif action == None:
    print(base.render())
