function footerBottom() {
    var t = $("footer"),
        e = $(".wrap"),
        i = $(window).height(),
        n = t.outerHeight(!0),
        o = e.outerHeight(!0) - e.height();
    e.css({
        "min-height": i - n - o
    })
}

function book_info(){
    
    var book_info_div = $('.book_info');
    var all_books_a = $('.all_books').find('a');

    all_books_a.on('click', function(){
        book_info_div.hide('slow');
        $('#' + $(this).html()).show('slow');
    });   
}

function genre_info(){
    var books_in_genre = $('.books_in_genre');
    var all_genres_a = $('.all_genres').find('a');

    all_genres_a.on('click', function(){
        books_in_genre.hide('slow');
        $('#' + $(this).html()).show('slow');
    });

}

$(document).ready(function() {
    footerBottom();
    book_info();
    genre_info();

    $(".wrap").resize(footerBottom);
    $(window).resize(footerBottom);


    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        items: 1,
    })


});


