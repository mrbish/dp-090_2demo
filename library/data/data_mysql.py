import pymysql
import models


conn = pymysql.connect(
    db='library',
    user='root',
    passwd='admin',
    host='localhost')
c = conn.cursor()


class Actions(models.ABCView):
    """docstring for info"""
    def get_all_genres():
        query = "select * from genre ORDER BY title"
        c.execute(query)
        rows = c.fetchall()
        data = []
        for genre_id, title in rows:
            genre = models.Genre(genre_id, title)
            data.append(genre)
        return(data)

    def add_genre(genre_title):
        query = "select * from genre"
        c.execute(query)
        rows = c.fetchall()
        for genre_id, title in rows:
            if title == genre_title: 
                print("<script type='text/javascript'>\
                    alert('You have this genre!');setTimeout(function()\
                    {window.location.href='/?action=get_all_genres';},100);</script>")
                break
        else:
            c.execute("INSERT INTO genre VALUES (null, '{genre_title}')"\
            .format(genre_title=genre_title))
            conn.commit()

    def delete_genre(genre_title):
        c.execute("DELETE FROM genre WHERE title = '{genre_title}'"\
        .format(genre_title=genre_title))
        conn.commit()

    def edit_genre_title(old_genre_title, new_genre_title):
        c.execute("UPDATE genre SET title = '{new_genre_title}' \
        WHERE title = '{old_genre_title}'".format(old_genre_title=old_genre_title, \
        new_genre_title=new_genre_title))
        conn.commit()

    def get_all_books():
        query = "select * from books ORDER BY title"
        c.execute(query)
        rows = c.fetchall()
        data = []
        for book_id, title, author, year, description, genre, genre_id in rows:
            book = models.Book(book_id, title, author, year, description, genre, genre_id)
            data.append(book)
        return(data)

    def add_book(data):
        query = "select * from books"
        c.execute(query)
        rows = c.fetchall()
        for book_id, title, author, year, description, genre, genre_id in rows:
            if title == data['book_title']:
                print('You have this book')
                break
        else:
            query = "INSERT INTO books VALUES (null, '{book_title}','{book_author}', \
                    '{book_year}', '{book_description}', '{book_genre}', '{genre_id}')" \
                    .format(**data)
            c.execute(query)
            conn.commit()

    def delete_book(book_title):
        c.execute("DELETE FROM books WHERE title = '{book_title}'"\
        .format(book_title=book_title))
        conn.commit()