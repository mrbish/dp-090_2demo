from abc import ABCMeta, abstractmethod

class Genre(object):
    """docstring for Genre"""

    def __init__(self, genre_id, title):
        self.__genre_id = genre_id
        self.__title = title

    @property
    def genre_id(self):
        return self.__genre_id

    @property
    def title(self):
        return self.__title

    # @title.setter
    # def title(self, new_title):
    #     self.__title = new_title.title()

    def __str__(self):
        return 'id: {genre_id} title: {title}'.format(genre_id=self.genre_id, title=self.title)


class Book(object):
    """docstring for Book"""

    def __init__(self, book_id, title, author, year, description, genre, genre_id):
        self.__book_id = book_id
        self.__title = title
        self.__author = author
        self.__year = year
        self.__description = description
        self.__genre = genre
        self.__genre_id = genre_id


    @property
    def genre_id(self):
        return self.__genre_id

    @property
    def genre(self):
        return self.__genre

    @genre.setter
    def genre(self, new_genre):
        self.__genre = new_genre.title()

    @property
    def title(self):
        return self.__title

    @property
    def author(self):
        return self.__author

    @property
    def year(self):
        return self.__year

    @property
    def description(self):
        return self.__description

    @property
    def book_id(self):
        return self.__book_id

    def __str__(self):
        return 'book_id: {0.book_id} \nTitle: {0.title} \nAuthor: {0.author}'\
        '\nYear: {0.year} \nDescription: {0.description} \nGenre: {0.genre} Genre id: {0.genre_id}'.format(self)


class ABCView(metaclass=ABCMeta):

    """docstring for View"""
    @abstractmethod
    def get_all_genres():
        raise NotImplementedError

    @abstractmethod
    def add_genre():
        raise NotImplementedError

    @abstractmethod
    def delete_genre():
        raise NotImplementedError

    @abstractmethod
    def edit_genre_title():
        raise NotImplementedError
